import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SinterklaasTest {

    Sinterklaas sk;

    @BeforeEach
    public void setup() {
        sk = new Sinterklaas();
        sk.players = new ArrayList<>();
    }

    @AfterEach
    public void after() {
        assertEquals(sk.players.size() * 3, allGiftsPresent());
    }

    @Test
    public void testGetFromNeighbors() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.players.add(new Player("test3"));
        sk.setNeighbors();

        for (Player p : sk.players) {
            p.gifts = 3;
        }

        sk.getFromNeighbors(sk.players.get(1)); //test2
        assertEquals(2, sk.players.get(0).gifts);
        assertEquals(5, sk.players.get(1).gifts);
        assertEquals(2, sk.players.get(2).gifts);
    }

    @Test
    public void testGiftToOtherPlayer() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.setNeighbors();

        for (Player p : sk.players) {
            p.gifts = 3;
        }

        sk.giftToOtherPlayer(sk.players.get(0));


        assertEquals(2, sk.players.get(0).gifts);
        assertEquals(4, sk.players.get(1).gifts);
    }

    @Test
    public void testSwapNeighbors() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.players.add(new Player("test3"));
        sk.setNeighbors();

        sk.players.get(0).gifts = 2;
        sk.players.get(1).gifts = 2;
        sk.players.get(2).gifts = 5;

        sk.swapNeighbors(sk.players.get(1));

        assertEquals(5, sk.players.get(0).gifts);
        assertEquals(2, sk.players.get(2).gifts);
    }

    @Test
    public void testAllGiftsTwoToRight() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.players.add(new Player("test3"));
        sk.players.add(new Player("test4"));
        sk.players.add(new Player("test5"));
        sk.players.add(new Player("test6"));
        sk.players.add(new Player("test7"));
        sk.players.add(new Player("test8"));
        sk.setNeighbors();

        sk.pile = 12;

        for (int i = 0; i < 4; i++) {
            sk.players.get(i).gifts = 3;
        }
        sk.allGiftsTwoToRight();

        assertEquals(0, sk.players.get(0).gifts);
        assertEquals(0, sk.players.get(1).gifts);
        assertEquals(3, sk.players.get(2).gifts);
        assertEquals(3, sk.players.get(3).gifts);
        assertEquals(3, sk.players.get(4).gifts);
        assertEquals(3, sk.players.get(5).gifts);
        assertEquals(0, sk.players.get(6).gifts);
        assertEquals(0, sk.players.get(7).gifts);
    }

    @Test
    public void testGrabFromPileWithoutGifts() {
        sk.pile = 3;
        sk.players.add(new Player("test1"));

        sk.grabFromPile(sk.players.get(0));

        assertEquals(1, sk.players.get(0).gifts);
        assertEquals(2, sk.pile);
    }

    @Test
    public void testGrabFromPileWithGifts() {
        sk.pile = 2;
        sk.players.add(new Player("test1"));
        sk.players.get(0).gifts = 1;

        sk.grabFromPile(sk.players.get(0));

        assertEquals(2, sk.players.get(0).gifts);
        assertEquals(1, sk.pile);
    }

    @Test
    public void testDonateLeastGiftsRightFromYouOnePerson() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.setNeighbors();

        sk.pile = 0;

        sk.players.get(0).gifts = 4;
        sk.players.get(1).gifts = 2;

        sk.donateLeastGiftsRightFromYou(sk.players.get(0));

        assertEquals(3, sk.players.get(0).gifts);
        assertEquals(3, sk.players.get(1).gifts);
    }

    @Test
    public void testDonateLeastGiftsRightFromYouLeftHasLess() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.players.add(new Player("test3"));
        sk.setNeighbors();

        sk.pile = 0;

        sk.players.get(0).gifts = 2;
        sk.players.get(1).gifts = 4;
        sk.players.get(2).gifts = 3;

        sk.donateLeastGiftsRightFromYou(sk.players.get(1));

        for (Player p : sk.players) {
            System.out.println(p);
        }

        assertEquals(3, sk.players.get(0).gifts);
        assertEquals(3, sk.players.get(1).gifts);
        assertEquals(3, sk.players.get(2).gifts);
    }

    @Test
    public void testDonateLeastGiftsRightFromYouLeftHasSame() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.players.add(new Player("test3"));
        sk.setNeighbors();

        sk.pile = 0;

        sk.players.get(0).gifts = 2;
        sk.players.get(1).gifts = 5;
        sk.players.get(2).gifts = 2;

        sk.donateLeastGiftsRightFromYou(sk.players.get(1));

        for (Player p : sk.players) {
            System.out.println(p);
        }

        assertEquals(2, sk.players.get(0).gifts);
        assertEquals(4, sk.players.get(1).gifts);
        assertEquals(3, sk.players.get(2).gifts);
    }

    @Test
    public void testDonateLeastGiftsRightFromYouPileNotEmpty() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.players.add(new Player("test3"));
        sk.setNeighbors();

        sk.pile = 3;

        sk.players.get(0).gifts = 1;
        sk.players.get(1).gifts = 4;
        sk.players.get(2).gifts = 1;

        sk.donateLeastGiftsRightFromYou(sk.players.get(1));

        for (Player p : sk.players) {
            System.out.println(p);
        }

        assertEquals(1, sk.players.get(0).gifts);
        assertEquals(4, sk.players.get(1).gifts);
        assertEquals(2, sk.players.get(2).gifts);
        assertEquals(2, sk.pile);
    }

    @Test
    public void testAllThreeToLeft() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.players.add(new Player("test3"));
        sk.players.add(new Player("test4"));
        sk.players.add(new Player("test5"));
        sk.players.add(new Player("test6"));
        sk.players.add(new Player("test7"));
        sk.players.add(new Player("test8"));
        sk.setNeighbors();

        sk.pile = 12;

        for (int i = 0; i < 4; i++) {
            sk.players.get(i).gifts = 3;
        }
        sk.allGiftsThreeToLeft();
        for (Player p : sk.players) {
            System.out.println(p);
        }

        assertEquals(3, sk.players.get(0).gifts);
        assertEquals(0, sk.players.get(1).gifts);
        assertEquals(0, sk.players.get(2).gifts);
        assertEquals(0, sk.players.get(3).gifts);
        assertEquals(0, sk.players.get(4).gifts);
        assertEquals(3, sk.players.get(5).gifts);
        assertEquals(3, sk.players.get(6).gifts);
        assertEquals(3, sk.players.get(7).gifts);
    }

    @Test
    public void testPutOnPile() {
        sk.pile = 2;
        sk.players.add(new Player("test1"));

        sk.players.get(0).gifts = 1;
        sk.putOnPile(sk.players.get(0));

        assertEquals(3, sk.pile);
        assertEquals(0, sk.players.get(0).gifts);
    }

    @Test
    public void testPutOnPileNoGifts() {
        sk.pile = 3;
        sk.players.add(new Player("test1"));

        sk.players.get(0).gifts = 0;
        sk.putOnPile(sk.players.get(0));

        assertEquals(3, sk.pile);
        assertEquals(0, sk.players.get(0).gifts);
    }

    @Test
    public void testswapWithPersonAcross() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));
        sk.players.add(new Player("test3"));
        sk.players.add(new Player("test4"));
        sk.players.add(new Player("test5"));
        sk.players.add(new Player("test6"));
        sk.players.add(new Player("test7"));
        sk.players.add(new Player("test8"));
        sk.setNeighbors();

        sk.pile = 12;

        for (int i = 0; i < 4; i++) {
            sk.players.get(i).gifts = 3;
        }

        sk.swapWithPersonAcross(sk.players.get(2));

        assertEquals(0, sk.players.get(2).gifts);
        assertEquals(3, sk.players.get(6).gifts);
    }

    @Test
    public void testSwapPresents() {
        sk.players.add(new Player("test1"));
        sk.players.add(new Player("test2"));

        sk.pile = 1;

        sk.players.get(0).gifts = 2;
        sk.players.get(1).gifts = 3;

        sk.swapPresents(sk.players.get(0));

        assertEquals(3, sk.players.get(0).gifts);
        assertEquals(2, sk.players.get(1).gifts);
    }


    public int allGiftsPresent() {
        int total = 0;
        for (Player p : sk.players) {
            total += p.gifts;
        }
        total += sk.pile;
        return total;
    }
}
