import java.util.ArrayList;

public class Sinterklaas {
    ArrayList<Player> players = new ArrayList<>();
    int onTheTurn = -1;
    int pile;
    int turns;
    int turnsafterpile;
    boolean pileEmpty;
    int open;
    int averageTurns;
    int averageOpen;
    int amount;

    public static void main(String[] args) {
        new Sinterklaas().run();
    }

    public void run() {
        amount = 10000;
        long start = System.currentTimeMillis();
        for (int i = 0; i < amount; i++) {
            players = new ArrayList<>();
            addPlayers();
            setNeighbors();
            pile = players.size() * 3;
            while (pile > 0 || turnsafterpile < 50) {
                if (pile == 0) {
                    pileEmpty = true;
                }
                if (pileEmpty) {
                    turnsafterpile++;
                }
                turns++;
                onTheTurn = (onTheTurn + 1) % players.size();
                switch (rollDice()) {
                    case 2 -> getFromNeighbors(players.get(onTheTurn)); //Je krijgt van beide buren een cadeautje
                    case 3 -> giftToOtherPlayer(players.get(onTheTurn)); // Geef 1 cadeau aan iemand anders
                    case 4 -> swapNeighbors(players.get(onTheTurn)); //Ruil de cadeaus van je buren met elkaar
                    case 5 -> open++; //Maak 1 cadeautje open (of wijs er een aan)
                    case 6 -> allGiftsTwoToRight(); //Alle cadeautjes 2 naar rechts
                    case 7 -> grabFromPile(players.get(onTheTurn)); //Pak 1 cadeautje (als alles op is: Steel 1 cadeau)
                    case 8 -> donateLeastGiftsRightFromYou(players.get(onTheTurn)); //Laat de persoon met de minste cadeaus rechts van jou een cadeau van jou pakken
                    case 9 -> allGiftsThreeToLeft(); //Alle cadeautjes 3 naar links
                    case 10 -> putOnPile(players.get(onTheTurn)); //Leg 1 cadeautje terug op de stapel
                    case 11 -> swapWithPersonAcross(players.get(onTheTurn)); //Wissel de cadeaus met de persoon tegenover je
                    case 12 -> swapPresents(players.get(onTheTurn)); //Ruil al je cadeautjes met iemand anders
                }
            }
//            printAll();
//            System.out.println("Turns: " + turns);
//            System.out.println("Open: " + open);
//            int total = 0;
//            for (Player p : players) {
//                total += p.gifts;
//            }
//            System.out.println("Total gifts: " + total);
            averageTurns += turns;
            averageOpen += open;
            open = 0;
            turns = 0;
            turnsafterpile = 0;
            pileEmpty = false;
        }
        long end = System.currentTimeMillis();
        averageTurns /= amount;
        averageOpen /= amount;
        System.out.println("Average turns: " + averageTurns);
        System.out.println("Average open: " + averageOpen);
        System.out.println("time in ms: " + (end - start));
    }

    public void addPlayers() {
        players.add(new Player("Jurgen"));
        players.add(new Player("Timo"));
        players.add(new Player("Niels"));
        players.add(new Player("Bart"));
        players.add(new Player("Mattijs"));
        players.add(new Player("Thomas"));
        players.add(new Player("Deklin"));
        players.add(new Player("Dennis"));
    }

    public int rollDice() {
        int random1 = (int) (Math.random() * 6 + 1);
        int random2 = (int) (Math.random() * 6 + 1);
        return random1 + random2;
    }

    public void swapNeighbors(Player p) {
        Player left = p.leftNeighbor;
        Player right = p.rightNeighbor;

        int temp = left.gifts;
        left.gifts = right.gifts;
        right.gifts = temp;
    }

    public void donateLeastGiftsRightFromYou(Player p) {
        Player least = new Player("temp");
        least.gifts = Integer.MAX_VALUE;
        ArrayList<Player> sadPlayers = new ArrayList<>();
        sadPlayers.add(least);
        for (Player player : players) {
            if (player.gifts == sadPlayers.get(0).gifts) {
                sadPlayers.add(player);
            } else if (player.gifts < sadPlayers.get(0).gifts) {
                sadPlayers = new ArrayList<>();
                sadPlayers.add(player);
            }
        }

        int closestCounter = Integer.MAX_VALUE;
        least = sadPlayers.get(0);
        for (Player sad : sadPlayers) {
            int counter = 0;
            Player current = p;
            while (!current.equals(sad)) {
                current = current.rightNeighbor;
                counter++;
            }
            if (counter < closestCounter) {
                least = current;
            }
        }

        if (pile > 0) {
            least.gifts++;
            pile--;
        } else {
            least.gifts++;
            p.gifts--;
        }
    }

    public void grabFromPile(Player p) {
        if (pile > 0) {
            pile--;
        } else {
            Player toRob = new Player("temp");

            //find someone with more than zero gifts to steal from
            while (toRob.gifts == 0) {
                int random = (int) (Math.random() * players.size());
                toRob = players.get(random);
            }

            toRob.gifts--;
        }
        p.gifts++;
    }

    public void putOnPile(Player p) {
        if (p.gifts > 0) {
            pile++;
            p.gifts--;
        }
    }

    public void getFromNeighbors(Player p) {
        int giftsToReceive = 2; //is initially two because we assume both neighbors can give a gift
        Player left = p.leftNeighbor;
        Player right = p.rightNeighbor;

        if (left.gifts == 0) {
            giftsToReceive--;
        } else {
            left.gifts--;
        }
        if (right.gifts == 0) {
            giftsToReceive--;
        } else {
            right.gifts--;
        }
        p.gifts += giftsToReceive;
    }

    public void giftToOtherPlayer(Player p) {
        if (p.gifts > 0) {
            int random = 0;
            while (random == players.indexOf(p)) {
                random = (int) (Math.random() * players.size());
            }
            Player player = players.get(random);
            player.gifts++;
            p.gifts--;
        }
    }

    public void allGiftsTwoToRight() {
        for (Player p : players) {
            p.previousgifts = p.gifts;
        }
        for (Player p : players) {
            Player player = p.rightNeighbor.rightNeighbor;
            player.gifts = p.previousgifts;
        }
    }

    public void allGiftsThreeToLeft() {
        for (Player p : players) {
            p.previousgifts = p.gifts;
        }
        for (Player p : players) {
            Player player = p.leftNeighbor.leftNeighbor.leftNeighbor;
            player.gifts = p.previousgifts;
        }
    }

    public void swapPresents(Player p) {
        int random = 0;
        while (random == players.indexOf(p)) {
            random = (int) (Math.random() * players.size());
        }
        int temp = players.get(random).gifts;
        players.get(random).gifts = p.gifts;
        p.gifts = temp;
    }

    public void swapWithPersonAcross(Player p) {
        Player across = p;
        for (int i = 0; i < players.size() / 2; i++) {
            across = across.rightNeighbor;
        }
        int temp = p.gifts;
        p.gifts = across.gifts;
        across.gifts = temp;
    }

    public void setNeighbors() {
        if (players.size() == 1) {
            players.get(0).leftNeighbor = players.get(0);
            players.get(0).rightNeighbor = players.get(0);
        } else if (players.size() == 0) {
            System.err.println("player list is empty!");
        } else {
            players.get(0).leftNeighbor = players.get(players.size() - 1);
            players.get(0).rightNeighbor = players.get(1);
            for (int i = 1; i < players.size() - 1; i++) {
                Player p = players.get(i);
                p.leftNeighbor = players.get(i - 1);
                p.rightNeighbor = players.get(i + 1);
            }
            players.get(players.size() - 1).leftNeighbor = players.get(players.size() - 2);
            players.get(players.size() - 1).rightNeighbor = players.get(0);
        }
    }

    public void printAll() {
        for (Player p : players) {
            System.out.print(p);
        }
    }
}
