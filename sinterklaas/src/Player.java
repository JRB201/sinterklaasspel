public class Player {
    public int gifts;
    int previousgifts;
    String name;
    Player leftNeighbor;
    Player rightNeighbor;

    public Player(String name) {
        this.name = name;
        this.gifts = 0;
    }

    public String toString() {
        return "[" + name + "]" + "->" + gifts + "\n";
    }
}
